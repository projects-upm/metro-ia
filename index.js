var nodes = [
  // Nodos
  {
    data: { id: "Beruni", line: "a", label: "Beruni" },
    classes: "center-left",
    position: { x: 200, y: 100 }
  },
  {
    data: { id: "Tinchlik", line: "a" },
    classes: "center-left",
    position: { x: 250, y: 150 }
  },
  {
    data: { id: "Chorsu", line: "a" },
    classes: "center-left",
    position: { x: 300, y: 200 }
  },
  {
    data: { id: "Gafur Gulom", line: "a" },
    classes: "center-left",
    position: { x: 350, y: 250 }
  },
  {
    data: { id: "Alisher Navoi", line: "a" },
    classes: "bottom-right",
    position: { x: 350, y: 300 }
  },
  {
    data: { id: "Uzbekistan", line: "a" },
    classes: "center-left",
    position: { x: 350, y: 350 }
  },
  {
    data: { id: "Kosmonavtlar", line: "a" },
    classes: "bottom-center",
    position: { x: 400, y: 400 }
  },
  {
    data: { id: "Oybek", line: "a" },
    classes: "bottom-center",
    position: { x: 450, y: 400 }
  },
  {
    data: { id: "Toshkent", line: "a" },
    classes: "bottom-center",
    position: { x: 500, y: 400 }
  },
  {
    data: { id: "Mashinasozlar", line: "a" },
    classes: "top-center",
    position: { x: 550, y: 400 }
  },
  {
    data: { id: "Dostlik", line: "a" },
    classes: "bottom-center",
    position: { x: 600, y: 400 }
  },

  // ROJO
  {
    data: { id: "Olmazor", line: "r", line: "r" },
    classes: "top-left",
    position: { x: 50, y: 600 }
  },
  {
    data: { id: "Chilonzor", line: "r", line: "r" },
    classes: "top-left",
    position: { x: 100, y: 550 }
  },
  {
    data: { id: "Mirzo Ulugbek", line: "r", line: "r" },
    classes: "top-left",
    position: { x: 150, y: 500 }
  },
  {
    data: { id: "Novza", line: "r", line: "r" },
    classes: "top-left",
    position: { x: 200, y: 450 }
  },
  {
    data: { id: "Milliy Bog", line: "r", line: "r" },
    classes: "top-left",
    position: { x: 250, y: 400 }
  },
  {
    data: { id: "Bunyodkor", line: "r", line: "r" },
    classes: "top-left",
    position: { x: 300, y: 350 }
  },
  {
    data: { id: "Pakhtakor", line: "r", line: "r" },
    classes: "top-left",
    position: { x: 350, y: 300 }
  },
  {
    data: { id: "Mustakillik Maydoni", line: "r", line: "r" },
    classes: "top-center",
    position: { x: 400, y: 300 }
  },
  {
    data: { id: "Amir Temur Hiyoboni", line: "r", line: "r" },
    classes: "top-right",
    position: { x: 450, y: 300 }
  },
  {
    data: { id: "Khamid Alimjan", line: "r", line: "r" },
    classes: "center-right",
    position: { x: 500, y: 300 }
  },
  {
    data: { id: "Pushkin", line: "r", line: "r" },
    classes: "center-right",
    position: { x: 550, y: 250 }
  },
  {
    data: { id: "Buyuk Ipak Yuli", line: "r", line: "r" },
    classes: "top-center",
    position: { x: 600, y: 200 }
  },

  // Verde
  {
    data: { id: "Shakhriston", line: "v" },
    classes: "center-left",
    position: { x: 450, y: 100 }
  },
  {
    data: { id: "Bodomzor", line: "v" },
    classes: "center-left",
    position: { x: 450, y: 150 }
  },
  {
    data: { id: "Minor", line: "v" },
    classes: "center-left",
    position: { x: 450, y: 200 }
  },
  {
    data: { id: "Abdula Kodiriy", line: "v" },
    classes: "center-left",
    position: { x: 450, y: 250 }
  },
  {
    data: { id: "Yunus Rajabiy", line: "v" },
    classes: "bottom-right",
    position: { x: 450, y: 300 }
  },
  {
    data: { id: "Ming Urik", line: "v" },
    classes: "top-left",
    position: { x: 450, y: 400 }
  },

  // Relaciones
  // Linea Azul
  { data: { id: "a1", source: "Beruni", target: "Tinchlik", line: "a" } },
  { data: { id: "a2", source: "Tinchlik", target: "Chorsu", line: "a" } },
  {
    data: { id: "a3", source: "Chorsu", target: "Gafur Gulom", line: "a" }
  },
  {
    data: {
      id: "a4",
      source: "Gafur Gulom",
      target: "Alisher Navoi",
      line: "a"
    }
  },
  {
    data: {
      id: "a5",
      source: "Alisher Navoi",
      target: "Uzbekistan",
      line: "a"
    }
  },
  {
    data: {
      id: "a6",
      source: "Uzbekistan",
      target: "Kosmonavtlar",
      line: "a"
    }
  },
  {
    data: { id: "a7", source: "Kosmonavtlar", target: "Oybek", line: "a" }
  },
  { data: { id: "a8", source: "Oybek", target: "Toshkent", line: "a" } },
  {
    data: {
      id: "a9",
      source: "Toshkent",
      target: "Mashinasozlar",
      line: "a"
    }
  },
  {
    data: {
      id: "a10",
      source: "Mashinasozlar",
      target: "Dostlik",
      line: "a"
    }
  },

  // Linea roja
  { data: { id: "r1", source: "Olmazor", target: "Chilonzor", line: "r" } },
  {
    data: {
      id: "r2",
      source: "Chilonzor",
      target: "Mirzo Ulugbek",
      line: "r"
    }
  },
  {
    data: { id: "r3", source: "Mirzo Ulugbek", target: "Novza", line: "r" }
  },
  {
    data: { id: "r4", source: "Novza", target: "Milliy Bog", line: "r" }
  },
  {
    data: { id: "r5", source: "Milliy Bog", target: "Bunyodkor", line: "r" }
  },
  {
    data: { id: "r6", source: "Bunyodkor", target: "Pakhtakor", line: "r" }
  },
  {
    data: {
      id: "r7",
      source: "Pakhtakor",
      target: "Mustakillik Maydoni",
      line: "r"
    }
  },
  {
    data: {
      id: "r8",
      source: "Mustakillik Maydoni",
      target: "Amir Temur Hiyoboni",
      line: "r"
    }
  },
  {
    data: {
      id: "r9",
      source: "Amir Temur Hiyoboni",
      target: "Khamid Alimjan",
      line: "r"
    }
  },
  {
    data: {
      id: "r10",
      source: "Khamid Alimjan",
      target: "Pushkin",
      line: "r"
    }
  },
  {
    data: {
      id: "r11",
      source: "Pushkin",
      target: "Buyuk Ipak Yuli",
      line: "r"
    }
  },

  // Linea Verde
  {
    data: { id: "v1", source: "Shakhriston", target: "Bodomzor", line: "v" }
  },
  {
    data: { id: "v2", source: "Bodomzor", target: "Minor", line: "v" }
  },
  {
    data: { id: "v3", source: "Minor", target: "Abdula Kodiriy", line: "v" }
  },
  {
    data: {
      id: "v4",
      source: "Abdula Kodiriy",
      target: "Yunus Rajabiy",
      line: "v"
    }
  },
  {
    data: {
      id: "v5",
      source: "Yunus Rajabiy",
      target: "Ming Urik",
      line: "v"
    }
  },

  // Transbordos
  {
    data: {
      id: "t1",
      source: "Pakhtakor",
      target: "Alisher Navoi"
    }
  },
  {
    data: {
      id: "t2",
      source: "Oybek",
      target: "Ming Urik"
    }
  },
  {
    data: {
      id: "t3",
      source: "Yunus Rajabiy",
      target: "Amir Temur Hiyoboni"
    }
  }
];

var styles = [
  {
    selector: "node",
    style: {
      width: "12px",
      height: "12px",
      content: "data(label)",
      "background-color": function(ele) {
        if (ele.data("line") == "a") {
          return "blue";
        }
        if (ele.data("line") == "r") {
          return "red";
        }
        if (ele.data("line") == "v") {
          return "green";
        }
        return "gray";
      },
      "border-color": "white",
      "border-width": "3px",
      color: function(ele) {
        if (ele.data("line") == "a") {
          return "blue";
        }
        if (ele.data("line") == "r") {
          return "red";
        }
        if (ele.data("line") == "v") {
          return "green";
        }
        return "gray";
      },
      "text-wrap": "wrap",
      "text-max-width": 20
    }
  },
  {
    selector: "edge",
    style: {
      "line-color": function(ele) {
        if (ele.data("line") == "a") {
          return "blue";
        }
        if (ele.data("line") == "r") {
          return "red";
        }
        if (ele.data("line") == "v") {
          return "green";
        }
        return "gray";
      },
      width: "5px"
    }
  },
  {
    selector: ".start",
    style: {
      height: "15px",
      width: "15px",
      "border-color": "gray"
    }
  },
  {
    selector: ".end",
    style: {
      height: "15px",
      width: "15px",
      "border-color": "gray"
    }
  },
  {
    selector: ".not-path",
    style: {
      opacity: "0.5"
    }
  },
  {
    selector: ".path",
    style: {
      "background-color": "black",
      "border-color": "black",
      "border-width": "3px",
      "line-color": "black"
    }
  },
  {
    selector: ".top-left",
    style: {
      "text-valign": "top",
      "text-halign": "left"
    }
  },
  {
    selector: ".top-center",
    style: {
      "text-valign": "top",
      "text-halign": "center"
    }
  },
  {
    selector: ".top-right",
    style: {
      "text-valign": "top",
      "text-halign": "right"
    }
  },
  {
    selector: ".center-left",
    style: {
      "text-valign": "center",
      "text-halign": "left"
    }
  },

  {
    selector: ".center-center",
    style: {
      "text-valign": "center",
      "text-halign": "center"
    }
  },

  {
    selector: ".center-right",
    style: {
      "text-valign": "center",
      "text-halign": "right"
    }
  },

  {
    selector: ".bottom-left",
    style: {
      "text-valign": "bottom",
      "text-halign": "left"
    }
  },

  {
    selector: ".bottom-center",
    style: {
      "text-valign": "bottom",
      "text-halign": "center"
    }
  },
  {
    selector: ".bottom-right",
    style: {
      "text-valign": "bottom",
      "text-halign": "right"
    }
  }
];

var $clear = $("#clear");
var $search = $("#search");

var $selectStart = $("#selectStart");
var $selectEnd = $("#selectEnd");

var start, end;

$("#selectStart").change(function() {
  cy.$().removeClass("start path not-path");
  console.log($(this).val());
  start = cy.nodes("#" + $(this).val()).addClass("start");
});

$("#selectEnd").change(function() {
  cy.$().removeClass("end path not-path");
  console.log($(this).val());
  end = cy.nodes("#" + $(this).val()).addClass("end");
});

$clear.on("click", clear);
$search.on("click", searchRoute);

var start, end;
var arrayOpt = [];
nodes.forEach(function(n) {
  if (n.position != undefined) {
    n.data.label = n.data.id.slice();
    n.data.id = n.data.id
      .split(" ")
      .join("_")
      .toLowerCase();

    arrayOpt.push({ id: n.data.id, label: n.data.label });
    n.position.x *= 2;
    n.position.y *= 2;
  } else {
    n.data.target = n.data.target
      .split(" ")
      .join("_")
      .toLowerCase();
    n.data.source = n.data.source
      .split(" ")
      .join("_")
      .toLowerCase();
  }
});
console.log(arrayOpt);
$.each(arrayOpt, function(key, value) {
  $selectStart.append(
    $("<option></option>")
      .attr("value", value.id)
      .text(value.label)
  );
  $selectEnd.append(
    $("<option></option>")
      .attr("value", value.id)
      .text(value.label)
  );
});

var cy = cytoscape({
  container: document.getElementById("cy"),
  elements: nodes,

  // Layout
  layout: {
    name: "preset"
  },

  minZoom: 0.5,
  maxZoom: 1.5,
  zoomingEnabled: true,
  userZoomingEnabled: true,
  panningEnabled: true,
  userPanningEnabled: true,

  selectionType: "single",

  style: styles
});

cy.edges().forEach(function(edge) {
  edge.data("weight", Math.floor(Math.random() * 6));
});

cy.nodes().lock();

cy.on("tap", "node", function(evt) {
  var node = evt.target;
  if (start == undefined) selectStart(node);
  else if (end == undefined) selectEnd(node);
});

var cy2 = cytoscape({
  container: document.getElementById("cy2"),

  // Layout
  layout: {
    name: "preset"
  },

  minZoom: 0.5,
  maxZoom: 1.5,
  zoomingEnabled: true,
  userZoomingEnabled: true,
  panningEnabled: true,
  userPanningEnabled: true,

  selectionType: "single",

  style: styles
});

function selectStart(node) {
  start = node;
  $("#selectStart").val(node.id());
  start.addClass("start");
}

function selectEnd(node) {
  end = node;
  $("#selectEnd").val(node.id());
  end.addClass("end");
}

function searchRoute() {
  if (start != undefined && end != undefined) {
    cy.startBatch();

    setTimeout(function() {
      var aStar = cy.elements().aStar({
        root: start,
        goal: end,
        weight: function(e) {
          console.log(e._private.data);
          return e.data("weight");

          // return e.data('is_bullet') ? 1 : 3; // assume bullet is ~3x faster
        }
      });

      if (!aStar.found) {
        clear();

        cy.endBatch();
        return;
      }

      cy.elements()
        .not(aStar.path)
        .addClass("not-path");
      aStar.path.addClass("path");

      cy.endBatch();

      var w = cy2.width(),
        y = cy2.height();
      var offset = 50;
      cy2.add(
        aStar.path.map(function(ele) {
          var e = ele.json();
          e.position = null;

          e.position = {
            x: (w * 3) / 4,
            y: 50 + offset
          };
          offset += 50;
          return e;
        })
      );

      cy2
        .$()
        .removeClass(
          "not-path path center-left center-center center-right bottom-left bottom-center bottom-right top-left top-center top-right"
        );
      cy2.nodes().addClass("center-left");
    }, 300);
  }
}

function clear() {
  $selectStart.val("");
  $selectEnd.val("");
  cy.elements().removeClass("path not-path start end");
  start = undefined;
  end = undefined;
  cy2.$().remove();
}
